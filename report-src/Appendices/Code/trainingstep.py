# Assuming program args are parsed'
# Assuming dataset is initialised'
# Assuming tensor operations added to the graph'
# Assuming mlp.py and es.py libraries imported'

params = PARSER.parse_args()
dataset = input_data.read_data_sets("MNIST_data/")

model = mlp.MLP(params, dataset)
optimiser = tf.train.GradientDescentOptimiser(params.alpha)
es = es.OpenAIES(model)

with tf.Session() as tf_sess:
  tf.global_variable_initializer().run()
  
  for i_epoch in range(params.epochs):
    
    # Keep a running average cost for the current epoch'
    cost_avg = 0
    
    # Traverse the entire training dataset in batches'
    for i_batch in range(params.batches):
      data_dict = make_dict(model.data.train.next_batch(batch_size)
      
      if params.bp:
        # Compute true gradients of the cost function'
        true_gradients = optimiser.compute_gradients(model.cost)
        
        # Take a gradient descent step. Equivalent to:'
        # train_step = optimiser.minimize(model.cost)'
        train_step = optimiser.apply_gradients(true_gradients)
        
      else if params.es:
        # Create offspring population by perturbing current weights'
        candidate_list = es.generate_population(model.get_weights())
        
        # Evaluate each offspring using Tensorflow'
        reward_list = np.zeros(params.mu)
        for i_candidate in range(params.mu):
          reward_list[i_candidate] = tf_sess.run(model.reward,
          					 feed_dict=data_dict)
          
        # Give the rewards to the ES algorithm to compute the result'
        estimated_gradients = es.compute_result(reward_llist)
        
        # Take a gradient descent step using estimated gradients'
        train_step = optimiser.apply_gradients(estimated_gradients)
      
      # Do the actual update of the Tensorflow trainable variables'
      tf_sess.run(train_step, feed_dict=data_dict)
        
        
        
  
  
  