import es

es = es.NES()

While time exists:

  candidate_list = es.get_population()

  fitness_list = np.zeros(es.POPULATION)

  for i in range(es.POPULATION):
    fitness_list[i] = evaluate(candidate_list[i])

  es.put_fitness(fitness_list)

  result = es.compute()