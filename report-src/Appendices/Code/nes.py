Inputs: 
$\theta_{0}$ // initial parameter vector 
$f$ // evaluation function 

While time exists do
    For k=1:$\lambda$ do
        Sample solution: $\mathbf { z } _{k}\sim \pi ( \cdot |\theta )$
        Evaluate the fitness:  $s_{k} \leftarrow f \left( \mathbf { z }_{k} \right)$
        Calculate log-derivatives: $\nabla _ { \theta } \log \pi \left( \mathbf { z } _ { k } | \theta \right)$
    $\nabla _ { \theta } J \leftarrow \frac { 1} { \lambda } \sum _ { k = 1} ^ { \lambda } \nabla _ { \theta } \log \pi \left( \mathbf { z } _ { k } | \theta \right) \cdot s_{ k })$
    $\mathbf { F } \leftarrow \frac { 1} { \lambda } \sum _ { k = 1} ^ { \lambda } \nabla _ { \theta } \log \pi \left( \mathbf { z } _ { k } | \theta \right) \nabla _ { \theta } \log \pi \left( \mathbf { z } _ { k } | \theta \right) ^ { T }$
    $\theta \leftarrow \theta + \eta \cdot \mathbf { F } ^ { - 1} \nabla _ { \theta } J$
