Inputs: 
$\theta_{0}$ \\ initial parameter vector
$f$ \\ evaluation function
$\lambda$ \\ population size
$\sigma$ \\ standard deviation of the normal distribution
$\alpha$ \\ step size
$optimiser$ \\ update rule

While time exists do
    For i=1:$\lambda/2$ do
        Sample perturbation: $\varepsilon _{i}\sim N\left( 0, \sigma\right)$
        Evaluate fitness: $s_{ i } \leftarrow f \left( \theta _ { t } + \varepsilon _ { i } \right)$
    Normalise score ranks $r_{i} \leftarrow rank( s ),r _ { i } \in [ 0,1)$
    Estimate gradient: $g \leftarrow \frac { 1} { \sigma * \lambda } \sum _ { i = 1} ^ { \lambda } \left( r _ { i } * \varepsilon _ { i } \right)$
    Update parameters: $\theta _ { t + 1} \leftarrow \theta _ { t } + optimizer( g, \alpha )$
