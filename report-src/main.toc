\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\contentsline {chapter}{Abstract}{iii}{section*.1}
\contentsline {chapter}{Acknowledgements}{v}{section*.2}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.4}
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.5}
\contentsline {section}{\numberline {1.2}Project Aim}{2}{section.6}
\contentsline {section}{\numberline {1.3}Related Work}{3}{section.9}
\contentsline {section}{\numberline {1.4}Report Structure}{4}{section.11}
\contentsline {chapter}{\numberline {2}Background}{5}{chapter.12}
\contentsline {section}{\numberline {2.1}Artificial Neural Networks}{5}{section.13}
\contentsline {section}{\numberline {2.2}Gradient-based Optimisation}{6}{section.17}
\contentsline {subsection}{\numberline {2.2.1}Gradient Descent Methods}{7}{subsection.18}
\contentsline {subsection}{\numberline {2.2.2}Backpropagation}{7}{subsection.19}
\contentsline {subsection}{\numberline {2.2.3}Problems and Limitations}{9}{subsection.22}
\contentsline {section}{\numberline {2.3}Evolution Strategy Optimisation}{9}{section.23}
\contentsline {subsection}{\numberline {2.3.1}Simple ES}{10}{subsection.25}
\contentsline {subsection}{\numberline {2.3.2}NES}{11}{subsection.43}
\contentsline {subsection}{\numberline {2.3.3}OpenAI-ES}{13}{subsection.60}
\contentsline {chapter}{\numberline {3}Development}{15}{chapter.78}
\contentsline {section}{\numberline {3.1}Project Management}{15}{section.79}
\contentsline {section}{\numberline {3.2}Project Design}{16}{section.83}
\contentsline {section}{\numberline {3.3}Project Implementation}{19}{section.96}
\contentsline {chapter}{\numberline {4}Experiments}{21}{chapter.168}
\contentsline {section}{\numberline {4.1}Hypothesis}{21}{section.169}
\contentsline {section}{\numberline {4.2}Exp.1: Fitness Functions}{24}{section.179}
\contentsline {section}{\numberline {4.3}Exp.2: Population Size}{26}{section.183}
\contentsline {section}{\numberline {4.4}Exp.3: Batch Stochasticity}{27}{section.186}
\contentsline {section}{\numberline {4.5}Exp.4: Comparison between ES and SGD}{28}{section.190}
\contentsline {section}{\numberline {4.6}Exp.5: SGD vs. SimpleES vs. NES vs. OpenAI-ES}{29}{section.192}
\contentsline {section}{\numberline {4.7}Discussion}{30}{section.194}
\contentsline {chapter}{\numberline {5}Conclusions}{31}{chapter.195}
\contentsline {section}{\numberline {5.1}Project Outcome}{31}{section.196}
\contentsline {subsection}{\numberline {5.1.1}Achievements}{32}{subsection.202}
\contentsline {subsection}{\numberline {5.1.2}Future Work}{33}{subsection.204}
\contentsline {section}{\numberline {5.2}Personal Outcome}{34}{section.205}
\contentsline {subsection}{\numberline {5.2.1}Technical and Scientific}{34}{subsection.206}
\contentsline {subsection}{\numberline {5.2.2}Self Optimisation}{34}{subsection.207}
\contentsline {chapter}{\numberline {A}Annex}{35}{appendix.216}
\contentsline {chapter}{\numberline {B}Index}{41}{appendix.223}
\contentsline {chapter}{References}{47}{appendix*.227}
