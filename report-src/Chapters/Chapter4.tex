% Chapter Template

\chapter{Conclusions} % Main chapter title

\label{Chapter4} % for referencing this chapter elsewhere, use \ref{ChapterX}

Overview. The final chapter of this report is meant to fulfil two functions. First one is to make explicit the principal two outcomes of this project: the product developed and the research conducted. This section re-asserts the adequacy of the experimentation framework and summarises the key findings of the research.The second function of this chapter is to express the relative magnitude and weight of the personal outcomes at the end of this project. Finally, an additional third function could be convincing the reader of future betterment - for both the science and the person behind this project.

%----------------------------------------------------------------------------------------
\section{Project Outcome}
The principal components of my project are the product and the research. They are structurally present in all aspects of this project, from personal objectives to project aim and final outcome\footnote{The foremost outcome of this project is perhaps its continued survival through an unprecedented amount of disturbance.}.

The project is in principle a dual-core ambition derived from two personal objectives I had set to achieve as a result of this project:
\begin{enumerate}
\item Gain a comprehensive understanding of the underlying concepts in Machine Learning as to enable the future of my interest in the field
\item Create a tangible product that brings a contribution to the field
\end{enumerate}

Building directly on top of my personal double-aim is the project double-aim, as formulated in the Introduction section:
\begin{enumerate}
\item Evaluate the suitability of ES optimisations for Neural Networks applied on the subset of supervised classification problems
\item Facilitate machine learning research by providing a reusable and user-extensible framework for automated experimentation with ES algorithms
\end{enumerate}

It should be clear that both aims have been achieved and that objectives at the highest level are all met. The lower level successful goals are listed in the ‘Achievements’ section below while unresolved objectives are presented together with unplanned functionality and ideas outside the scope of this project, in the 'Future Work' section.

Before discussing any details, I would like to clearly state that I am proud of all my achievements. I was on top of a new wave in Machine Learning with the opportunity to contribute to its surge with practical usefulness. I think I have achieved that through facilitating  new research with the framework and laying the groundwork for it with the results of my experiments. Regardless of the troubled time span of this project, I met most of my personal goals, but because of the troubled time span of this project, I set myself many more.


%------------------------------------

\subsection{Achievements}

To further emphasise the dual-nature of the project, I group achievements into two:

\textbf{Scientific Method and Research}
\\
\hrule
\begin{itemize}
\item Successful unconventional training of a neural network
\item Empirical evaluation and relative comparison of multiple training methods
\item Investigation into the feasibility of ES optimisation as an alternative to Backprop
\item Proof of concept for the applicability of ES in supervised classification: achieving 91\% MNIST accuracy with a simple version of ES
\item Improvement brought to an existing algorithm (from 81.79 to 85.14\% - this is statistically significant\footcite{https://en.wikipedia.org/wiki/Statistical_significance})
\end{itemize}

\textbf{Product Development and Software Engineering}
\\
\hrule
\begin{itemize}
\item Experimentation framework with a command line user interface
\item Application of design principles for software architecture
\begin{itemize}
\item Modularity
\item Reusability
\item Extensibility
\end{itemize}
\item System integration:
\begin{itemize}
\item Selective employment of Tensorflow in the parts of the training process that cause significant speedup when optimised, such as doing a forward pass or pre-processing large data.
\item Use of Tensorboard to provide automatic visualisation with my framework.
\item Demonstration of integrable interface with OpenAI Gym
\end{itemize}
\end{itemize}

%------------------------------------

\subsection{Future Work}

\textbf{1. Parallelising Tensorflow with MPI} As previously explained when describing the OpenAI algorithm \ref{<openaies>}, the crucial feature of ES and the reason it sparked interest is its constitution for distributed parallelism. The ‘parallelism’ part is because the process of estimating is naturally parallelisable - i.e. evaluating independent set of parameters for any given model. The ‘distributed’ part is possible due to the insignificant communication overhead that comes from a clever implementation where workers share a noise table which they all use to generate individuals. Thus, every worker has rapid access to the information needed to generate another worker’s candidate. This property allows for large scale implementations and potentially linear speedups.

\textit{Why?} The first reason is because the training time of a sequential ES algorithm is in the order of hours on demanding hyperparameters. This is imposing severe restrictions on both the type and the number of experiments that are feasible in the period of time allocated for this project. The second reason is that the whole idea of ES for supervised learning (but not only) is that if we are 1000x slower, we can easily just use 1000 more cores to compensate. 
This is the hypothesis for a potentially fascinating experiment  and I am still determined to pursue this idea either in continuation of this project or in other future projects.

\textit{Why not yet?} Parallelisation was a crucial initial requirement for the project, for the two reasons mentioned above. This objective was ongoing at the time of submission but was not included in the codebase.

\textbf{2. Applying ES on RL tasks and/or other non-supervised learning} RL Agent acts as a layer of abstraction between the MLP model (agent’s brain) and the OpenAI Gym environment in which they would take actions, receive rewards and observe changes. The scope is for them to maximise the reward for that environment.

\textit{Why?} My vision for this project was ultimately applying ES in place where Backpropagation is not applicable (e.g. when there are no ‘correct answers’, or the answers are fed back with delay or when the objective function is non-differentiable). RL tasks are some of the interesting problems to consider for ES.

\textit{Why not yet?}. Turns out that Reinforcement Learning does not suffice with a quick read in order to grasp and this project was already involving extensive amounts of laborious study due to calculus and statistical maths required to understand gradient backpropagation and evolution strategies. However, this objective is somehow ongoing and some of the basic requirements for it are already met. The framework architecture depicted in \ref{fig:SystemArchitecture} shows how the Agent class is interacting with the rest of the framework. Note that I did not implement any RL algorithms myself, but used external code \cite{baselines} in order to demonstrate the quality of extensibility for my framework. See a running environment in Figure \ref{fig:RLExample} of the Appendix.

\textbf{3. Framework User Documentation} This is an important requirement for the success of my ES framework considering it is aimed at researchers to use. 

%----------------------------------------------------------------------------------------
\section{Personal Outcome}

%------------------------------------
\subsection{Technical and Scientific}

\begin{itemize}
\item Research skills: digesting scientific papers
\item New mathematics arsenal - calculus and statistics
\item Strong knowledge base of core machine learning
\begin{itemize}
\item neural networks and deep learning
\item gradient-based optimisation 
\item evolution strategy algorithms
\end{itemize}
\item Experiment design (ensuring results are meaningful and reliable)
\item New programming paradigms: Graph Computation
\item Technologies: Tensorflow, Tensorboard, numpy, OpenAI Gym
\item Methodologies: Data-Driven Planning, Spiral Development
\end{itemize}

%------------------------------------
\subsection{Self Optimisation}

I identified five disciplines which I have either acquired or improved as a result of this project. These are sorted by the \textit{'individual organic gradient'\footnote{Note: I did not check but I might have invented this term.} (IOG)}\footnote{Please credit me in any future IOG-ES optimisation work.}  of my intrinsic fitness function with respect to a lesson learned'\footnote{i.e. how much impact it had on me, personally.}
\begin{enumerate}
\item \textbf{Time} -- 
“To do two things at once is to do neither.“ (Publilius Syrus)
\item \textbf{Vision} --
"He who has a ‘why’ to live for can bear with almost any ‘how’." (Friedrich Nietzsche)
\item \textbf{Planning} --
"Let our advance worrying become advance thinking and planning." (Winston Churchill)
\item \textbf{Change control} --
"Just because something doesn't do what you planned it to do doesn't mean it's useless." (Thomas Edison)
\item \textbf{Risk and Failure} --
"I have not failed. I've just found 10,000 ways that won't work." (Thomas Edison)
\end{enumerate}

%----------------------------------------------------------------------------------------
%\section{Future Work}
