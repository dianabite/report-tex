% Chapter Template

\chapter{Introduction} % Main chapter title

\label{Chapter0} % for referencing this chapter elsewhere, use \ref{Chapter0}

Overview. This first chapter of this report aims to provide a succinct overview of this project. The opening section strives to show the reader the merit of this project and the motivation behind it. The second section explains the purpose, the aims and objectives of this project. The following section provides a placing of this project in the higher context by informing about the ongoing work in a newborn field. The final section gives an overview of how this report is shaped and organised. That is to say, this chapter tackles the questions of ‘Why?’, ‘How?’ and ‘What else?’ one after the other before it provides an outline of the whole report structure in the final section. 

%----------------------------------------------------------------------------------------
\section{Motivation}

Until very recently, the idea that Evolution Strategy can rival Stochastic Gradient Descent in Deep Learning was of no prospect. The first move in the direction of dismissing this assumption came from OpenAI \cite{2017arXiv170303864S} who showed that a simple evolutionary algorithm can rival state-of-the-art gradient-based deep reinforcement learning (RL) approaches on the benchmarks that the field is currently evaluated on, the Mujoco and Atari learning environments. This has caused surprise and controversy within the research community. It is important to note that evolution has a long history in Reinforcement Learning with significant stretches of time being the leading method on popular benchmarks (see the NEAT algorithm \cite{stanley:ec02}, CoSyne \cite{gomez:ecml06} and LEEA \cite{Morse:2016}). However, the ability of deep learning began to revolutionise many applications of Artificial Intelligence, including Reinforcement Learning (see DeepMind in 2015 \cite{human} and 2016 \cite{Mnih:2016aa}. By that means, the surprising part is simply that evolution and large networks are actually compatible, dismantling any previous assumptions, thereby opening up Deep Learning to the tools of evolutionary computation.

Research in Artificial Intelligence appears to be following the trend of repurposing old ideas when technological constraints loosened as we achieved bigger computational power and accumulated a large amount of data. This phenomenon is what has been pushing the field forward for decades and it is the story of some of the most groundbreaking advancements in Machine Learning (see AlexNet \cite{NIPS2012_4824} in computer vision or CD-DNN-HMMs \cite{Yu:2012:CST:3042573.3042574} in speech recognition). 

Historically, major instrumental advancements have happened when researchers decided to question the fundamental assumptions. It is plausible that Evolution Strategy is the next in line to confirm the historic trend and my personal opinion is that this is not far in the future.


%----------------------------------------------------------------------------------------
\section{Project Aim}

Following from the previous section, it seems like the innovative OpenAI paper \cite{2017arXiv170303864S} marks the beginning of a new line of research. I am therefore very excited to dedicate this project to facilitating what could potentially be the discovery of new prospects for the progress of Deep Learning.

There are two \textit{principal components} to my project:
\begin{enumerate}
\item The Research - evaluate the suitability of ES optimisations for Neural Networks applied on the subset of supervised classification problems
\item The Product - facilitate machine learning research by providing a reusable and user-extensible framework for automated experimentation with ES algorithms
\end{enumerate}

Working towards achieving this double-aim requires meeting the following objectives:
\begin{itemize}
\item \textbf{Study} of neural network optimisation methods:
\begin{itemize}
\item master statistical mathematics and calculus
\item understand the backpropagation algorithm and its limitations
\item understand how evolution strategies can be used for optimisation
\item assimilate new concepts, terms and conventions in the ES literature
\end{itemize}
\item \textbf{Implementation} of optimisation algorithms
\begin{itemize}
\item general purpose multi-layer neural network (MLP) in Tensorflow
\item stochastic gradient descent with backpropagation
\item three ES variations: a simple one, NES and the OpenAI-style ES
\end{itemize}
\item \textbf{Development} of experimentation framework
\begin{itemize}
\item command line interfacing
\item design for reuse and user-extensibility
\item Tensorboard integration for plotting and visualisation
\end{itemize}
\item \textbf{Deployment} of the framework developed to conduct MNIST-based experiments intended to uncover the relation between ES and SGD and provide new insight about the capabilities of ES algorithms on supervised problems.
\end{itemize}
\pagebreak
%----------------------------------------------------------------------------------------
\section{Related Work}

When this project began, the recent revelation from OpenAI \cite{2017arXiv170303864S} that a simple evolutionary algorithm is a promising rival to modern gradient-based reinforcement learning was the only paper released that had a real practical demonstration supported by empirical results. Since then, work in parallel with mine has already been published. I had to return to this section of the report multiple times to add another new publication that just came out. Uber AI Labs released three arXiv publications which further support the already-emerging realisation. I have summarised the key findings of each one below. 

\textbf{“Deep Neuroevolution: Genetic Algorithms Are a Competitive Alternative for Training Deep Neural Networks for Reinforcement Learning”} \cite{DBLP:journals/corr/abs-1712-06567} employs a conventional population-based genetic algorithm on hard RL problems and successfully evolves a Deep Neural Network (DNN) with more than four million parameters. Rather suggestively, this network is referred to as “Deep GA” - the largest neural network trained by traditional evolution\footnote{i.e. does not estimate gradients.}. I think this reinforces the idea that optimisation by following the gradient may not always be the ideal choice. Its performance is benchmarked on the popular Atari task of the OpenAI Gym framework \cite{DBLP:journals/corr/BrockmanCPSSTZ16}. Results show that the GA can achieve the potential of both ES and the modern deep reinforcement learning algorithms DQN (based on Q-learning) \cite{human} and A3C (based on policy gradients) \cite{Mnih:2016aa}, but it is better parallelisable and thus faster. 

\textbf{"Safe Mutations for Deep and Recurrent Neural Networks through Output Gradients”} \cite{DBLP:journals/corr/abs-1712-06563} demonstrates performance improvement by
applying techniques recently developed in ES research. It addresses the concern of random perturbations in large dimensions causing problems such as variance between runs and local minima convergence. I am particularly excited about this as it confirms that integrating new advancements within Evolutionary Computation into Deep Learning creates the space for pushing the cutting-edge of AI forward. 

\textbf{“ES Is More Than Just a Traditional Finite-Difference Approximator”} \cite{DBLP:journals/corr/abs-1712-06568} explains the key distinction between ES optimisers and finite difference methods. Instead of optimising for the optimal solution, ES optimises for the optimal \textit{distribution} of solutions. This method has been compared to the classic finite-difference approximation, from the mathematical point of view. This paper states - theoretically \textbf{and} empirically - that ES is a gradient estimator that does not optimise for just the reward (as if applying finite-difference approximation on the reward in the parameter space); but for reward of the entire population as an average. The experimental conclusion from here is that ES prioritises robustness to perturbation. This distinction has interesting implications that still require investigation.\\
%----------------------------------------------------------------------------------------
\section{Report Structure}

The reporting of the entire project is structured into five chapters, organised as follows:
\begin{itemize}
\item Chapter 1: introduces the reader to the problem placed in the context of ongoing machine learning research and justifies the usefulness of the project inside this context.
\item Chapter 2: provides the theory required for the understanding of the project, aiming to be comprehensive but succinct.
\item Chapter 3: covers the details of the more practical aspects of the project, starting from how it has been managed (methodology), then designed (architecture) and finally implemented (code).
\item Chapter 4: demonstrates the usage of the framework through a series of MNIST-based experiments meant to give insightful comparisons between the performance of ES optimisation in contrast with Backpropagation.
\item Chapter 5: serves as a conclusion by highlighting the technical achievements as well as the personal ones; discusses both the present and the future.
\end{itemize}
