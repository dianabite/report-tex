% Chapter Template

\chapter{Development} % Main chapter title

\label{Chapter2} % for referencing this chapter elsewhere, use \ref{ChapterX}

Overview. This chapter covers the main aspects of a project: how it is managed, what design principles have been applied and which tools and techniques are used to implement it. The Management section introduces the development methodology and the planning process when faced with dynamic constraints. The Design section provides an architecture review as well as a brief on the the software engineering practices employed. The Implementation section describes the development environment, discusses the choice of programming language and auxiliary libraries and justifies the use of specific tools and technologies: Tensorflow, Tensorboard and OpenAI Gym.

%----------------------------------------------------------------------------------------
\section{Project Management}

The fundamental characteristic of this project is that a \textbf{flexible scope} must be achieved in a \textbf{fixed time} while facing \textbf{volatile constraints}. Taking that into consideration in addition to the unpredictable nature of the project circumstances determined me to adopt an agile methodology\Footcite{https://en.wikipedia.org/wiki/Agile_software_development} called “The Spiral Model”  (Boehm, 1988) \cite{Boehm:1986:SMS:12944.12948}. This is a 'progressive' iterative model in the sense that every iteration is progressing through the same sequence of steps on all levels of the project. Each cycle ends with a review meant to validate or invalidate the feasibility of future objectives. This is illustrated in the picture listed below\footnote{Image from \href{https://en.wikipedia.org/wiki/Software_development_process}{Wikipedia.}}. 

\begin{figure}
\centering
\includegraphics[scale=0.61]{spiral.png} 
\caption[Spiral Development]{The Spiral Development Methodology.}
\label{fig:Spiral}
\end{figure}

The spiral-shaped approach proved to fit perfectly with the double-scope of the project. Researching \textit{and} developing code is generally a continuous iteration in constant need of review and reconsideration. It is also a source of unpredictable constraint, but I can only speak for myself on that. 

Spiral development is particularly focused on minimising the project risks by breaking it up into smaller pieces. It prompts for risk assessment and provides more leverage for change in the course of the project.

This has provided me a with framework for managing the critical aspects of this project. Or at least it encouraged me to try. 


%----------------------------------------------------------------------------------------
\section{Project Design}

I began this project by defining the phases it would go through in its lifetime (see Appendix \ref{fig:Phases}). This is a standard approach of mine and more or less all my projects go through the same phases: 
\begin{itemize}
\item \textbf{Phase 0 -- Understand:} 
this is where I set the knowledge foundation for the problem I am trying to solve \\\\
For this project, this phase was plain difficult. Gaining in-depth understanding of the difficulties in training neural networks required tremendous amount of calculus and statistical mathematics to be grasped and I even took an advanced probability module last semester to help me. I only had the Machine Learning background that came as a result of the second year module and so I had to start from scratch implementing a network and printing matrix shapes everywhere. Once I \textit{think} I know what I'm talking about, I need to make sure that I really do so I look for consultation.
\item \textbf{Phase 1 -- Scope:}
this is where I decide and define what the solution will look like \\\\
This phase usually involves tying out software tools, prototyping ideas and comparing alternatives. Once I know what the options are, I can proceed to think about the requirements. I have a bad habit of manually imposing requirements. For example, I technically did not have to use the Tensorflow library, but I wanted to learn it and so I made it a requirement. The way I normally go about setting the real requirements is through dissecting user stories that I continuously track in a spreadsheet. Once I have the functionalities extracted, I attempt a massive task breakdown and put the details in the plan.
\item \textbf{Phase 2 -- Develop:} 
this is where I actively work towards solving the problem \\\\
Development phase also includes Testing and Validation. For projects in need of a thorough process I split this into two, but this project involves extensive benchmarking which, I think, is good enough a substitute if the results are compared against a known correct method.
\end{itemize}

I set the highest priority to the following requirements:
\begin{enumerate}
\item implementation of a re-usable 1-hidden layer neural network
\item es.py library to provide at least three ES algorithms
\item command line interface to the library
\item ability to combine models and training algorithms in arbitrary ways
\item ability to add new algorithms and models to the framework
\item automatic graph visualisation in Tensorboard
\item ability to use Tensorflow in combination with external optimisers
\item metric reporting
\item reasonable logging information and error-reporting
\end{enumerate}

I enforce three major code design principles:
\begin{itemize}
\item modularity (separated components do not depend on each other)
\item reusability (easy to adjust for use in closely-related situations)
\item extensibility (interfaced interactions enable easy plug-in of external component)
\end{itemize}

Furthermore, I commit to code linting\Footcite{https://en.wikipedia.org/wiki/Lint_(software)} and frequent versioning with Git \cite{wilson-software-carpentry-2006}.

The development view\Footcite{https://en.wikipedia.org/wiki/View_model} of the program architecture is described in the Unified Model Language (UML) below.

\begin{figure}
\centering
\includegraphics[scale=0.55]{sysarch.png} 
\caption[Development View]{Development View.}
\label{fig:SystemArchitecture}
\end{figure}

%----------------------------------------------------------------------------------------
\section{Project Implementation}

Several technologies merged together in the making of this project.

\begin{description}

 \item [Tensorflow.] Google's open sourced computational graph library is the most popular\Footcite{http://bigdata-madesimple.com/top-10-machine-learning-frameworks/} deep learning framework today. It is widely used in both research and industry and as I stated before in this report, learning how to use Tensorflow was a project goal in itself. This is an essential element in the skill set I set out to build as a result of this project. The imposed requirement on using Tensorflow dictated my choice for the programming language and auxiliary packages.


\item [Tensorboard.] This is a visualisation tool that extends Tensorflow. The pretty graphs are my favourite part of this project and so I integrated this tool with my framework to meet the requirement for automatic visualisation\footnote{i.e. user of the framework sees graphs without having to write code for storing and plotting data.}.This has taken from the development time and redirected it for research. See Annex \ref{fig:tb1} and \ref{fig:tb2}.

\item[OpenAI Gym.] Open source library and interface\footcite{https://gym.openai.com/} to reinforcement learning tasks. It provides a multitude of wide-ranging environments for evaluating RL Agents. Enables users to focus on developing the underlying algorithm instead of the benchmarking process. This integrates nicely with my framework, which enables users to change algorithms and conduct structured experiments.

\item [Python.] Because I am using Tensorflow, the programming language was not really a choice. The Python API is nice and fully supported. On top of that, Python is increasingly popular in the whole of Data Science, almost competing with the historical giants Matlab and R. The multitude of python packages out there is another major advantage. For this project however, I have only used \textbf{numpy}\Footcite{http://www.numpy.org/}, \textbf{scikit-learn}\Footcite{http://scikit-learn.org/stable/} and \textbf{matplotlib}\Footcite{https://matplotlib.org/}. The \textbf{Jupyter}\Footcite{https://jupyter.org/} interface was heavily used for prototyping ideas and learning maths. 

\end{description}

The framework is build around the training loop interface depicted in the code below (\ref{<esopt>}). It is straightforward to use and makes it simple to implement any training algorithms. This has allowed me to easily plug in implementations of multiple ES variations and compare them by changing just one line.

\begin{algorithm}
\begin{lstlisting}[language=Pseudocode]  % Start your code-block

from es import OpenAIES, SimpleES, NES
model = mlp.MLP(params, dataset)
#es = es.OpenAIES(model)'
#es = es.SimpleES(model)'
es = es.NES(model)
\end{lstlisting}
\end{algorithm}

\begin{algorithm}
\caption{Interfaced training process}
\label{<esopt>}
\lstinputlisting[language=Pseudocode,mathescape=true]{Appendices/Code/trainingstep.py}
\end{algorithm}
 




