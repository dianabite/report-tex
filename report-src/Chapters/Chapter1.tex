% Chapter Template

\chapter{Background} % Main chapter title

\label{Chapter1} % for referencing this chapter elsewhere, use \ref{ChapterX}

Overview. This chapter introduces the reader to some of the key concepts within the project, but because of how popular some of them are, it also assumes minimum familiarity with terms in Machine Learning. Therefore the first section is only briefly an introduction and more like an update on the current state of the science. Then follows my research into the types of function optimisers, with particular focus on comparing gradient-based versus evolution algorithms.
%----------------------------------------------------------------------------------------
\section{Artificial Neural Networks}

Neural Networks are currently dominating the field of Machine Learning, but this has not always been the case. Training large Artificial Neural Networks (ANNs) is intrinsically difficult \cite{279181} and research in this field started in the 80s, but it was not so long ago when the rise of Deep Learning \cite{deepai} caused a Neural Network Renaissance. The Backpropagation algorithm \cite{5231496} is what powers Deep Neural Networks \cite{DBLP:journals/corr/abs-1106-4574}. The effectiveness of this method is enabled by sufficient computation power and large amounts of training data. This realisation has opened the door for many new advancements which led to very rapid progress in AI. This algorithm is now at the core of our most intelligent systems, solving some of the more complex problems out there like computer vision, natural language processing (NLP) and automatic speech recognition (ASR).

As the name suggests, neural networks are inspired by the mental activity hypothesis which states that thinking is happening when groups of neurons in a network ‘activate’, fire and cause the activation of other groups of neurons.

Below is a representation\footnote{Image is from \cite{Russell:2003:AIM:773294}} of the fundamental component of a neural network. 

\begin{figure}
\centering
\includegraphics[scale=0.15]{neuron}
\caption[Representation of a neuron]{A mathematical representation of the neuron.}
\label{fig:Neuron}
\end{figure}

For simplicity, we can think of a neuron as ‘a thing’ that holds a number. For each neuron, this number will be the weighted sum of all its inputs received from the previous to which we add some bias. A sigmoid function is applied to this linear combination in order to ensure the capacity for modelling non-linear functions. We applied non-linearity to the preactivation value in order to get the unit’s activation.\footnote{The activations of the first layer are the input features of the training data.}. Activations in one layer are determined by the activation of the previous layer. So in a fully connected network, each neuron contributes it’s activation to all the neurons in the next layer. This contribution is proportional to the connection strength (weight). 

Training a neural network means that the network has the right parameters (that is weights and biases) to give good predictions most of the time. A randomly parameterised network is untrained. 
We expect a trained network to receive our input and make a prediction based on that - pass it through the network, computing all the activations, and return an answer - the final layer’s output. This is called a forward pass. In order to train a network, we need a way to tell the network about the quality of its answers and to do that we define a cost function. I like to think of the cost function as a level of complexity on top of the neural network function. This takes as inputs the weights (the current parameters of the network) and spits out a single number which reflects how fit those parameters are (over many many training examples). Once we have a metric for how good or bad the network is, we need to tell it how to change in order improve (i.e. lower the cost). One way to express this problem would be: “Find the input which minimises the value of this cost function.” It is an optimisation problem. 

The following section outlines the major optimisation approaches. 

%----------------------------------------------------------------------------------------
\section{Gradient-based Optimisation}

The gradient of a function measures the variation induced by a very small change in input, also known as \textit{instantaneous rate of change}. Consider any cost function $C ( \theta )$ that we want to minimise. When $C$ is fed a vector of parameters it returns a scalar that represents the metric for evaluation of the input. The formal definition for the gradient of this function with respect to the parameter $\theta$ is:

$$\frac { \partial C ( \theta ) } { \partial \theta } = \lim _ { \delta \theta \rightarrow 0} \frac { C ( \theta + \delta \theta ) - C ( \theta ) } { \delta \theta }$$

Examples of gradient method are the gradient descent and the conjugate gradient. The latter does not concern this project.

\pagebreak
%----------------------------------------
\subsection{Gradient Descent Methods}

In theory, we can find the minimum (or maximum) or a function by solving $\frac { \partial C ( \theta ) } { \partial \theta } = 0$. In practice, that is rarely even possible.

However, the gradient is nothing but a multi-dimensional slope. The negative gradient of a function gives you the direction of the steepest descent. So one approach to minimising a function is computing its gradient, taking a step in the direction of the steepest descent and repeating that over and over. This is called ‘gradient descent’. It’s a way to converge towards a local minima of a function.

There are many variations, but the canonical approach (also called  \textbf{Batch Gradient Descent}) is described by the following equation: 
$$\theta ^ { k + 1} = \theta ^ { k } - \alpha \frac { \partial C \left( \theta ^ { k } \right) } { \partial \theta ^ { k } }$$
A significantly faster improvement to the batch version is the \textbf{Stochastic Gradient Descent (SGD)}, which instead of computing the gradient of the cost function for the entire dataset, it computes the gradient from just one example (or more examples in the case of \textbf{Mini-Batch Stochastic Gradient Descent}). 

The difference between batch-GD and stochastic-GD is basically how much information contributes to the computation of the gradient. Evidently, this influences the accuracy of the gradient. In the first case, the computation results in the exact value, or what is called ‘the true gradient’; however, this requires the differentiation of the cost function evaluated on \textit{all} training input making it rather slow. For the second case of SGD, the computation is only an approximation of the gradient, computed from a small subset of examples, but significantly faster. This enables a more frequent (but noisy) update to the parameters, causing a faster convergence. 


%----------------------------------------
\subsection{Backpropagation}

Assuming the knowledge from the previous sections, we already know what gradients are and how to use them to take a step towards minimising some function. The question of how to compute them is about to be answered. Backpropagation is an algorithm for computing gradients in neural networks. Backpropagation was popularised by Hinton in “Learning representations by back-propagating errors” (1986) \cite{Rumelhart:1988:LRB:65669.104451} and many say it marked the dawn of the Deep Learning era. 

Without diving into the mathematics, a high-level description of backpropagation is this: we use calculus to assign neurons a proportion of the blame for any prediction mistake made at the output layer. This ‘blame’ value is called a ‘delta’ and every node’s delta is dependent on the deltas of all the nodes in the layer above. So you start computing the errors of the output layer and then backpropagate that error through the network. See a detailed pseudocode\footnote{This pseudocode is from Norvig \cite{Russell:2003:AIM:773294}}  description below.

\begin{algorithm}
\caption[Backpropagation]{Backpropagation for learning in multilayer networks}
\label{alg1}
\addtocontents{loa}{\vskip 6pt}
\includegraphics[scale=0.7]{backprop}
\end{algorithm}

%----------------------------------------
\subsection{Problems and Limitations}

Deep Learning is based on large datasets comprised of 'a situation' associated with its correct categorisation (e.g. image and categorisation, speech and transcript). Pattern recognition and Perception are both important tasks for the challenge of AI and supervised learning models built via deep learning are very powerful, but that is not everything we want our AIs to be able to do. We want AI to take actions in complicated environments and acomplish goals. However, a lot of these tasks in the world do not have a correct answer (or if they do, they are not immediately known e.g. driving a car, mobile robot navigating, playing a game) - in such domains learning is based on exploration: you try stuff and see what works.

For these applications and many others, Backpropagation with gradient descent is either impossible or very ineffective. This is because gradients are impossible to compute in the absence of an algebraic model of the optimisation function (as in the case of many experimental systems). Furthermore, issues arise when the cost function is non-differentiable (i.e. presents discontinuities).
%----------------------------------------------------------------------------------------
\section{Evolution Strategy Optimisation}

Evolution Strategies (ES) - or Evolutionary Strategies as it is sometimes referred to - are a subset of Evolutionary Algorithms (EA), which is a type of black box optimisation. The setup for the process is that some numbers go in (e.g. parameters of a neural network) and then another number comes out to report the condition of the input. If we want to find the best setting of the inputs, what that means is that we are optimising a function with respect to its input, but without the knowing or assuming the structure of the function. 

There are many variants of ES but they the share a common ground: each offspring \textit{solution} is evaluated based on a fitness function and assigned a fitness score. In the following generation, the algorithm produces a new population taking into consideration those scores. This process repeats until some stopping criteria is met.

In ES literature, the convention is that algorithms are referred to by using a special notation  which encodes family characteristics of the algorithm. The general form is 
$\left( \mu / \rho \ +, \lambda \right)$-ES which describes the following:
\begin{itemize}
\item $\mu$-parameter denotes the size of the parent population
\item $\rho$-parameter is optional and sometimes omitted; it suggests that from the $\mu$ parents in the current generation, only $\rho$ parents will count towards the next
\item $\lambda$-parameter denotes the size of the offspring population
\item $+$ means that offspring compete with the parents in the next generation (they are added to the population) 
\item $,$ implies POLO\footnote{POLO = Parents Only Live Once (I don't expect losing marks for attempting humour, or should I?)} meaning that parents get discarded on every generation
\end{itemize}

So for example in $\left( 1, \lambda \right)$-ES the best one out of the $\lambda$ offspring survives to serves as the parent of the next generation and the current one is discarded. \\ The $(1+1)$-ES is an 'elitist selection scheme' with one parent and one offspring, each competing with each other on every generation.

In my research, I chose to study three different types of evolution strategy. Note that my choice of algorithms is is naturally subjective. It is based on personal curiosity and preference. Other methods have comprehensive historical overviews available here \cite{Beyer2002} and here \cite{article}.

I start with the simplest form of ES, which was also the first algorithm I implemented in the very beginnings of this project. Here I will also introduce the fundamental concepts in ES. Building from there, I will try to explain NES \cite{JMLR:v15:wierstra14a} before I touch on the specifics of the OpenAI-ES algorithm. 

What follows is a technical overview of the three methods I studied. 

The set up is defined as follows: 
\begin{itemize}
\item The solution search space is the continuous domain $\mathbb{R} ^{n}$
\item A solution is a n-dimensional vector $x$
\item The objective is to minimise a function $f:\mathbb{R} ^{n}\rightarrow \mathbb{R} ,x\rightarrow f\left( x\right)$ ($f$ can evaluate all $x$ but there is no other assumption)
\item The problem is generating vectors $x$ with minimal values for $f$ while keeping the number of evaluations small.
\end{itemize}

\subsection{Simple ES}

The simplest $\left( \mu, \lambda \right)$-ES algorithm is sometimes also referred to as \textit{'the canonical ES'} \cite{Chrabaszcz:2018aa}. The approach is straightforward: for the first generation, initialize a random solution vector $\theta_{0}$ of size $\mu$ and for each subsequent generation, spawn $\lambda$ offspring by sampling from the normal distribution $\varepsilon _{i}\sim N\left( 0,\sigma ^{2}\right)$. Each sample is a perturbation we apply on the parameter vector $\theta_{0}$ which results in $\lambda$ new candidates. Each one gets evaluated on the objective function, but only the top $\mu$ highest scoring vectors are collected and combined into a weighted mean to form next generation's parent $\theta_{1}$. 

\begin{algorithm}
\caption[Simple Evolution Strategy]{Simple ES}
\addtocontents{loa}{\vskip 6pt}
\label{<simplees>}
\lstinputlisting[language=Pseudocode,mathescape=true]{Appendices/Code/ses.py}
\end{algorithm}

The weight recombination formula is from \cite{6513313}.

Note that despite the word 'evolution', ES has very little to do with biological evolution because technically, there are no parents (no crossover and no mutations). However, one could, on an abstract level, still view the process as generating individuals in a population without using the concept of parents, but by applying statistics instead, i.e. sampling from a probability distribution which is updated at each generation. This way, ES is much closer to Monte Carlo or other statistical methods, but because it is actually a population based method, it falls under the general class of evolutionary algorithms.

\subsection{NES}

Natural Evolution Strategies (NES) is a relatively new \cite{JMLR:v15:wierstra14a} black-box optimization technique. The algorithm builds on top of a special case of REINFORCE \cite{Williams1992} .The main principle is similar to many evolution strategy algorithms, that is to keep a probability distribution from which to sample potential solutions to be evaluated. From these samples, NES then estimates a gradient on the parameters. This gradient can then be used in conjunction with any modern optimiser (Adam, RMSProp, Momentum, etc). 

What makes NES special is that the it follows another kind of gradient - the \textit{natural gradient} (first introduced in \cite{doi:10.1162/089976698300017746}); this has shown to have multiple advantages over the plain gradient.

Because the plain gradient follows the steepest descent on the actual model parameters, a change in the parameters of the distribution leads to different gradients. This is causing undesired effects such as oscillation and premature convergence. The crucial feature of the natural gradient is lacking the parameterisation dependency. NES removes it by optimising the \textit{expectation} of fitness.
This is very similar to optimising the fitness of the entire population.

The way I’d like to explain this is by analogy to political ideologies\footcite{Please do not assume my adherence to any}. Imagine you had to build an artificial ‘perfect society’ simulator. So you have to come up with learning algorithms to apply on your initial population of pre-human creatures with primitive brains. For simplicity, it’s required that the population size must remain constant throughout generations. Now imagine scenarios where you optimise for the individual as opposed to the population. Firstly, ‘your people’ need to learn survival skills so you reward behaviour that keeps the individual alive. A local minima in this situation is someone realising that if they kill someone else it increases their chances of survival so you end up with a society of murderers. But then maybe ‘the cheater gene’ (where nobody gets any mischievous ideas) does not occur in your one run of the simulation. Still, rewarding individual success is likely to create funny societies in which the richest 1\% own half the world's wealth and few productive people enable subsistence for the lazy and the poor. On the other hand, if you optimise for the well-being of the entire population, you are jeopardising the maximum performance achievable in your society. That is because you are not investing in your top-achievers, but you are likely to create a Marxist\footnote{see "The Communist manifesto by Karl Marx and Friedrich Engels." \cite{nla.cat-vn2038114}} utopian society.


Let $F$ be the objective function for candidates in a probability distribution with parameters $z$ and $\theta$. Let $z$ be a solution sample. The \textit{expected value} of $F$ is:
$$J ( \theta ) = E _ { \theta } [ F ( z ) ] = \int F ( z ) \pi ( z ,\theta ) d z$$

We want to compute the gradient of  $J ( \theta )$ with respect to $\theta$ (see \cite{JMLR:v15:wierstra14a} details on the log-likelihood mathematics):

$$\nabla _ { \theta } J ( \theta ) = E _ { \theta } \left[ F ( z ) \nabla _ { \theta } \log \pi ( z ,\theta ) \right]$$

If we know solutions $z ^ { 1} ,z ^ { 2} ,\ldots z ^ { N }$, the expression given above is equivalent to the sum: 
$$ \nabla _ { \theta } J ( \theta ) \approx \frac { 1} { N } \sum _ { i = 1} ^ { N } F \left( z ^ { i } \right) \nabla _ { \theta } \log \pi \left( z ^ { i } ,\theta \right) $$

Once we have a gradient, we can pick any update rule (SGD, Adam, etc), choose a step size $\alpha$ and start updating the $\theta$ parameters using the common rule $\theta \rightarrow \theta + \alpha \nabla _ { \theta } J ( \theta )$. Continue until some criteria is met.

\begin{algorithm}
\caption{Natural Evolution Strategy}
\label{<nes>}
\addtocontents{loa}{\vskip 6pt}
\lstinputlisting[language=Pseudocode,mathescape=true]{Appendices/Code/nes.py}
\end{algorithm}

\subsection{OpenAI-ES}

The version of ES employed in the prominent ‘Evolution Strategies as a Scalable Alternative to Reinforcement Learning’ paper“ \cite{2017arXiv170303864S} relates very closely to NES.
For every generation $t$, the initial set of parameters $\theta_{t}$ is applied a random noise vector $\varepsilon_{i}$ with $(1\leq i \leq \lambda)$ where $\lambda$ is the offspring population size. 
The noise is sampled from the normal distribution $N ( 0,\sigma )$ and simply added to the value of the current parameter vector: $\theta _ {} + \varepsilon _ {i}$. Evaluating this new set of parameters will give the reward $r_{i}$ of the population. In the paper, OpenAI employ a center-ranked reward \cite{2017arXiv170303864S}. 

Gradient estimation is based on the collective reward: 
$$g  = \frac { 1} { \lambda \sigma } \sum _ { i = 1} ^ { \lambda } \varepsilon_{i} r _ { i }$$

This algorithm is actually a simplification of the NES algorithm presented earlier where the standard deviation $\sigma$ of the normal distribution is kept constant on every generation. This choice is justified as they also introduced a novel parallelization technique in which communication between two ‘evaluator’ nodes only involves two numbers: the evaluation score and the random seed of the noise table. That way, workers can re-generate other workers’ solution vectors, which is significantly cheaper for the system.
\begin{algorithm}
\caption{OpenAI Evolution Strategy}
\addtocontents{loa}{\vskip 6pt}
\label{<openaies>}
\lstinputlisting[language=Pseudocode,mathescape=true]{Appendices/Code/openaies.py}
\end{algorithm}