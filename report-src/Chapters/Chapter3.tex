% Chapter Template

\chapter{Experiments} % Main chapter title

\label{Chapter3} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

Overview. This chapter reports in detail the experiments that were conducted and how the developed framework was used. It explains the experiment setup, the processes involved and the dataset used. It also provides an analysis of the results and it attempts to speculate about the determining factors. The overall conclusion and the potential implications associated are summarised in the final section of this chapter.

%----------------------------------------------------------------------------------------
\section{Hypothesis}

\textbf{Aim.} This series of experiments is an attempt to lay the groundwork for assessing effectiveness of supervised learning with Evolution Strategies as an alternative to the traditional Stochastic Gradient Descent with Backpropagation. The aim is to provide basic insight into the relation between the two paradigms by exploring into various aspects of the supervised training process.   

\textbf{Dataset.} MNIST \cite{Lecun98gradient-basedlearning} is the most popular dataset in the field of ML and the original source\footcite{http://yann.lecun.com/exdb/mnist/}  still serves as the benchmark of supervised learning algorithms. However, it is a tradition in the community that all machine learning algorithms are first tested on MNIST. It has become the ‘Hello World!’ of Machine Learning.
MNIST is comprised of 60 000 training examples and 10 000 testing examples.

\textbf{Algorithm.} For the remainder of this report, I use the term \textbf{'ES'} to refer to a specific training algorithm based on the OpenAI method (Alg.\ref{<openaies>}) as presented in paper \cite{2017arXiv170303864S}. The only differences are: instead of using the ADAM optimiser, I am using the standard \textit{'delta rule'}\footcite{https://en.wikipedia.org/wiki/Delta_rule} and I do not employ any fitness shaping (such as reward ranking that OpenAI uses, which is particularly useful for rewards in the RL world, not so much in worlds of perfect information). Instead, I use the raw values given by the fitness function. In this case, the fitness function is the negative cross-entropy on the training input (usually a batch of 64 examples). I chose to study this one in particular firstly because it is naturally simple and secondly because it is naturally parallelisable. In a parallel implementation, each candidate in the population can be evaluated by an independent worker and computations can be distributed among very large networks without any limits imposed by the communication bandwidth available. This is possible because, unlike parallelised backpropagation, workers do not communicate entire vectors (sizes commonly exceeding one million) of computed gradients. It is only necessary for workers to share two scalar values: the fitness of the individual and the random seed used to generate it. This is already making parallelisation significantly more practical, but in addition to that, workers may not even need to send the noise perturbations if they share a common noise table with the master; thus workers only need to send a small index value. This is illustrated in Figure \ref{fig:Parallel}.

\textbf{Process.} Training is done in batch-iterations over the entire training set. For example, a 2000 iterations training with a batch size of 50 means that the optimiser will perform 1200 parameter updates per iteration (1 update for each batch) and $2000 * 1200$ gradient updates in total. We already know that ES is a gradient estimator, which means that for each batch in the input data, instead of computing the true gradient (on labeled datasets that is the exact result of a numerical computation), ES makes an approximation\footnote{Reminder: based on all offspring generated from $\theta$ with noise sampled from a normal distribution.}. This makes is possible to have similar if not identical training processes for both SGD and ES\footnote{Figure \ref{fig:Train} briefly illustrates the ES training process.}.

\textbf{Framework.} All my experiments are designed with the help of the experimentation framework developed as part of this project. Training is implemented with the interface provided by the framework. This has enabled me to ensure a consistent process going from SGD to ES and provide reliability in my results. The python code in Alg.\ref{<esopt>} demonstrates the use of the interface. Note that when training with ES the gradients are computed outside Tensorflow, but the evaluation of each candidate is a tensor operation doing a forward pass. Similarly, the GradientDescentOptimizer is doing the actual updates to the variables.

\textbf{Evaluation.} 
I generally measure \textit{model+optimiser} performance using \textit{evaluation accuracy}. This is the computed \textit{occasionally}\footnote{User-configurable setting. Usually every 10, 50, 100 or 500 training epochs.} based on the classification error of the \textit{entire test set}. Most graphs use this metric plotted against the number of iterations. For some optimisation aspects, other metrics (e.g. training accuracy, average cost) are more relevant. This is made explicit when the case.

\textbf{Hyperparameters.}  Values have been pre-determined by randomly searching the ${(\sigma, \alpha, \mu)}$ space. Figure \ref{fig:hypers} shows a small subset of the search space. Unless otherwise specified, all of the experiments below use hyperparameters listed in the table \ref{tab:hypers} below. Note that these are not necessarily the absolute best performing parameters, but a compromise between training performance and training time.


\begin{table}[h]
\centering
\begin{tabular}{l*{6}{c}l}
Description & Symbol & Value \\
\hline
Population size & $\mu$ & $200$    \\
Standard deviation of the normal distribution & $\sigma$ & $0.005$   \\
Learning rate & $\alpha$ & $0.001$  \\
Batch Size (as used in the Caffe MNIST tutorial\Footcite{https://caffe.berkeleyvision.org/gathered/examples/mnist.html}) & & $64$  \\
Optimiser: vanilla GradientDescent using Delta Rule &  &   \\
Cost: cross-entropy | Fitness: negative cross-entropy&  & \\
\hline
\end{tabular}
\caption[Hyperparameters]{Hyperparameters symbols and values. The learning rate $\alpha$ corresponds to the size of the update, the $\sigma$-parameter corresponds to the 'area' of the space in which it's possible to take that step and the population size $\mu$ represents the size (dimensionality) of that space.} 
\label{tab:hypers}
\end{table}

\pagebreak

\textbf{Network Architecture.} I use the model below for all my experiments. 
\begin{figure}[h]
\captionsetup{width=.9\linewidth}
\centering
\includegraphics[scale=0.6]{networkarch} 
\caption[Network Architecture]{Network Architecture. This is 784x32x10 MLP (255 000 parameters).}
\label{fig:netarch}
\end{figure}

\begin{figure}[h]
    \centering
    \captionsetup{width=.9\linewidth}
    \begin{minipage}{0.65\textwidth}
        \includegraphics[width=0.95\textwidth]{g-hypers}
    \end{minipage}\hfill
    \begin{minipage}{0.35\textwidth}
        \includegraphics[width=0.9\textwidth]{l-hypers} 
    \end{minipage}
    \caption[Exp.0: Hyperparameter Search]{Training accuracy for different ES parameters. In order to explore as many combinations as possible for the purpose of finding suitable hyperparameters, population is reduced to 100 and training length is capped at 2000 iterations. It can be seen that, much like SGD, large learning rates cause difficulties in convergence, especially in conjunction with large $\sigma$ values.}
    \label{fig:hypers}
\end{figure}

\textbf{Other Considerations.} 
It is difficult to ensure equality of opportunity for algorithms when many factors are involved (e.g. interest from the community, age, benefits from new technology, etc). In my effort on minimising the contributing variables \textit{and} maximising the number of experiments in the limited time available, I imposed the following constraints:
\begin{itemize}[noitemsep,nolistsep]
\item Omit any algorithm optimisations, including regularisation
\item Disable Tensorflow from potentially parallelising any tensor operations.
\item Stopping criteria is based on iteration number instead of convergence factor
\end{itemize}

\textbf{Run Environment.} macOS 10.12.6 and Python 3.6.3.

%----------------------------------------------------------------------------------------
\section{Exp.1: Fitness Functions}

Probably the first question to ask when considering an evolution algorithm is \textbf{'What makes a population fit?'}.

In reinforcement learning, actions are associated with a score and the reward used for learning is a function of this score. It generally requires an average of multiple agent rollouts (trials). The objective is \textit{maximising the reward function}. In contrast, supervised learning is based on the opposite idea of \textit{minimising a cost function}. Options are plentiful and varied\footcite{https://goo.gl/6FZyva}, but the most commonly used measures of cost are: Classification Error (CE), Mean Squared Error (MSE) - also known as Sum of Squared Errors (SSE) - and Cross Entropy Error (XE). The table below provides a very condensed comparison between the three as applied in classification. See \cite{costfct} for an in-depth review.
 
\begin{table}[h]
\captionsetup{width=.9\linewidth}
\centering
\begin{tabular}{|l|c|c|c|c|c|c|}
 & Prediction & Target & ? & \begin{tabular}[c]{@{}c@{}}Classification\\ Error (CE) \end{tabular} & \begin{tabular}[c]{@{}c@{}}Mean Squared\\ Error (MSE)\end{tabular} & \begin{tabular}[c]{@{}c@{}}Cross Entropy \\ Error (XE)\end{tabular} \\
\hline
\multicolumn{1}{|c|}{\multirow{3}{*}{I}} & 0.3 0.3 0.4 & 0 0 1 & Y & \multirow{3}{*}{0.33} & \multirow{3}{*}{0.81} & \multirow{3}{*}{1.38} \\
\multicolumn{1}{|c|}{} & 0.3 0.4 0.3 & 0 1 0 & Y &  &  &  \\
\multicolumn{1}{|c|}{} & 0.1 0.2 0.7 & 1 0 0 & N &  &  &  \\
\hline
\multirow{3}{*}{II} & \multicolumn{1}{l|}{0.1 0.2 0.7} & 0 0 1 & Y & \multirow{3}{*}{0.33} & \multirow{3}{*}{0.34} & \multirow{3}{*}{0.64} \\
 & \multicolumn{1}{l|}{0.1 0.7 0.2} & 0 1 0 & Y &  &  &  \\
 & \multicolumn{1}{l|}{0.3 0.4 0.3} & 1 0 0 & N &  &  & 
\end{tabular}
\caption[Error measures]{Performance comparison of two models ($I$ and $II$) using three cost measures evaluated on three training data items. Prediction is a softmax probability.}
\label{tab:costs}
\end{table}

On a first glance, the two models described above appear to be equally correct (both I and II manage 2 out of 3 correct predictions, implying a CE score of $0.33$), but they are obviously not equally competent. Model I barely predicts the first two training examples and gets the third one very wrong. In comparison, model II makes two confident correct predictions and only just misses the third one. Thus we can conclude that \textit{classification error is too crude a measure.} The other errors are better measures that take into consideration how close the prediction gets to the target. This is providing more granular insight into the cost of a model and XE is considered better than MSE (the superior model gets assigned a significantly lower error score).

Note that it is important to make the distinction between cost during the training process and cost after training, with the latter being using to get an estimate of the model effectiveness. 

Suppose we are doing supervised training using backpropagation. As we know, the backpropagation algorithm computes exact gradient values derived from error. The choice of MSE or XE changes the equation equation for the gradients. For mathematical reasons, using CE for training is impossible (backpropagation begins by computing the partial derivative of the error function, which is always zero for CE). However, in the testing phase, \textit{classification error (CE) best represents how good an estimator the model is.}

This first experiment tests the effectiveness of the cost functions converted into fitness functions (i.e. as used in Evolution Strategy). In all cases, the conversion means negating the cost value. That way, maximising the fitness is bringing values closer to 0 from the negative side. I expect cross-entropy to provide a better training experience.

If we want to see how good the model is when employing the different fitness functions, we look at the evaluation (testing) accuracy. If we want to see how the model learns on different fitness functions, we look at the training accuracy.
 \begin{figure}[h]
      \captionsetup{width=.9\linewidth}
    \centering
    \begin{minipage}{0.95\textwidth}
    	\includegraphics[scale=0.5]{g-reward2} 
	\includegraphics[scale=0.5]{g-reward} 
	\end{minipage}\hfill
    \begin{minipage}{0.05\textwidth}
    \includegraphics[scale=0.7]{l-reward} 
    \end{minipage}
    
    	    	\caption[Exp.1: Comparison of fitness functions]{Comparing fitness functions by looking at both the testing (top graph, averaged over 5 runs) accuracy and the training (bottom graph, also averaged over 5 runs) accuracy. The CE fitness is best, followed by XE and MSE.}
   		 \label{fig:Exp11}
\end{figure}

\textbf{Discussion.} It came as a surprise that CE appears to be the best fitness here. It was not obvious to me from the start that the advantages of the XE and MSE cost measures do not translate to ES. Although loss granularity provides insightful information when calculating exact gradients, it holds no weight for the ES estimations. That is intuitive on the idea that the classification error is ultimately what a makes a good model. Even more, model evaluation is actually done on the testing set classification error.
It becomes perhaps obvious that optimising for accuracy will yield better accuracy and that the learning curve growing slowly will eventually dominate non-accuracy-based fitness functions.
\pagebreak
%----------------------------------------------------------------------------------------
\section{Exp.2: Population Size}

Knowing which fitness is best for evolving a population\footnote{Remember that we are solving a supervised classification problem.}, the question that follows is 'How big a population is the optimal size?'. This experiment aims to uncover any relation between population size and gradient quality while being severely restricted by the hour-long training times. This is because the framework does not support any parallel implementations yet; therefore,  this experiment is also an attempt at striking the optimal balance between performance and time.

 \begin{figure}[h]
    \centering
    \begin{minipage}{0.5\textwidth}
		\includegraphics[scale=0.45]{g-popcost} 
		\includegraphics[scale=0.46]{g-popacc} 
    \end{minipage}\hfill
    \begin{minipage}{0.1\textwidth}
		\includegraphics[scale=0.7]{l-popcost} 
    \end{minipage}
	\caption[Exp.2: Comparison of population sizes]{Cross entropy cost function (top graph) with the corresponding testing accuracy (bottom graph) when training on ES with different population sizes. SGD baseline performance is provided.}
    \label{fig:Exp21}
\end{figure}

\textbf{Discussion.} At first sight, the estimated gradient approaches the true gradient in the limit as the population size increases, assuming small perturbations. In other words, the bigger the population, the better the gradient approximation (represented by the cost curves in the top graph). This relation has a direct effect on the model accuracy.

For the sake of comparison, the OpenAI application employs a population size of 10000. Evaluating such a big population would be infeasible unless the natural parallelisation of the algorithm is exploited. The costs of doing so is of no concern for today's large-scale technological enterprises, often deploying much more than 10000 CPUs.

\pagebreak
%----------------------------------------------------------------------------------------
\section{Exp.3: Batch Stochasticity}

The most commonly used gradient descent method is the mini-batch gradient descent (the same method described in the experiment hypothesis). This is a compromise between having a fully stochastic gradient (computed from 1 example, very fast but not very accurate) or a 'true gradient' (computed from \textit{all} the training examples, accurate but very slow). It turns out that the stochasticity introduced is beneficial for the training process, but in the case of ES, this is already present in the generation of the population, so perhaps there is no need for that here. This experiment aims to either confirm or disprove that statement.
 
Because it is still computationally infeasible to evaluate each offspring on the entire training set, we evaluate each one on a unique random subset. 

 \begin{figure}[h]
        \begin{minipage}{0.3\textwidth}
             \captionsetup{width=.99\linewidth}
		 \caption[Random Batch Training Loop]{This represents one training iteration for both modes. It uses the color legend as in the graph below. In theory, each gradient estimation is now based on a more comprehensive sampling of the training set.}
    \label{fig:Exp33}
    \end{minipage}
    \begin{minipage}{0.7\textwidth}
    \centering	 
    	\includegraphics[scale=0.62]{batchstoc}
    \end{minipage}\hfill
\end{figure}

 \begin{figure}[h]
     \captionsetup{width=.9\linewidth}
    \centering	 
    	\includegraphics[scale=0.5]{g-batch} \\
		\includegraphics[scale=0.7]{l-batch}
 \caption[Exp.3: Random Batch ES]{Testing accuracy increases from 81.79 to 85.14\%.  Additionally, the learning curve is a much smoother, suggesting better coping with noise during training. These results are averaged over 5 runs.}
    \label{fig:Exp31}
\end{figure}

\textbf{Discussion.} I am happy to have made a significant marginal gain\footcite{Credits to Sir David John Brailsford CBE (https://www.bbc.co.uk/news/magazine-34247629)}. It seems that the added stochasticity from the mini-batches is harming ES performance.
\pagebreak

%----------------------------------------------------------------------------------------
\section{Exp.4: Comparison between ES and SGD}

On the baselines established in the previous experiments, it becomes more appealing to finally attempt a thorough performance comparison between ES and SGD. This is THE experiment and it builds upon all previous ones, meaning that ES training here is done on the highest performing fitness, it uses random batches and a population size of 350. These new settings are causing an increase in the duration of one ES iteration from 15s to 45s. For this experiment, the iteration cap is relaxed to 5000 and the training process is repeated 5 times. The two algorithms are compared on the same number of iterations. One iteration corresponds to one gradient update (either true or estimated). Obviously the wall time is not a topic of consideration until large-scale parallelism is accessible. 

\begin{figure}[h]
\centering
\includegraphics[scale=0.55]{g-sgdes} 
\includegraphics[scale=0.9]{l-sgdes} 
\caption[Exp.4: SGD vs. ES]{Testing accuracy over (almost) 5000 iterations. Shows how ES performance compares to the plain Gradient Descent with Backpropagation. These results are averaged over 5 runs.}
\label{fig:Exp4}
\end{figure}

\textbf{Discussion.} ES is capable of achieving a respectable 91.10\% on a supervised learning problem. Note that this algorithm is still a relatively simple one, stripped of any optimisations. I think this alone is intriguing enough as it raises quite a diverse set of questions, all waiting eagerly to be researched and answered. For example:

How big does the population need to be such that ES becomes as good as SGD? Unfortunately this is outside the realms of my possibilities, but a topic for future work nevertheless.

An interesting investigation case would be whether there is a formal relation between the estimated gradient and the true gradient that can be mathematically described and measured?

\pagebreak

%----------------------------------------------------------------------------------------
\section{Exp.5: SGD vs. SimpleES vs. NES vs. OpenAI-ES}

All experiments so far are focusing the OpenAI-inspired ES algorithm. This last experiment provides context for all the three variations of ES described in the Background section: SimpleES, NES and OpenAI NES. The aim is to build a rough picture of how the algorithms relate to one another and how that related to the SGD baseline. For that purpose, I am looking at the training process instead of testing accuracy. 

To get good accuracy, the numbers of iterations for this experiment has been increased to 5000. Because training with ES algorithms takes a considerable amount of time and there are three of these algorithms in use here, this experiment has not been repeated. 
 
\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{g-all} \\
\includegraphics[scale=0.9]{l-all} 
\caption[Exp.5: SGD vs. NES vs. OpenAI-ES vs. SimpleES]{Smoothed training accuracy across 3000 iterations. Provides a context for the performance of various ES variations, not just the OpenAI one. SGD is used as a baseline for the comparison. Note that these results are based on single run.}
\label{fig:Exp5}
\end{figure}

\textbf{Discussion.} 

SGD is expected to severely outperform any of these algorithms but NES is showing promising potential. The difference in performance between the original NES and the OpenAI-ES variation of it is considerable and that is supposedly because restricting to keeping the $\sigma-parameter$ constant results in the algorithm losing its fine-tuning abilities. 

Perhaps the most surprising thing is that even the simplest version of ES can achieve a first class accuracy (if enough time is available for training).
\pagebreak
%----------------------------------------------------------------------------------------
\section{Discussion}

This set of experiments answers a series of questions aimed at understanding how ES estimations relate to SGD gradients. Furthermore, Experiment 3 shows how a simple trick can make a small improvement on the performance of ES.

The most important insight gained as a consequence of these experiments, I think, is that the potential of ES increases with the size of the population. With only 5000 offspring, ES goes beyond the 90\% accuracy mark on the MNIST task. This is not when and where ES capabilities are best advertised.
Supervised learning benefits from knowing the true gradient (per batch, that is also the optimal gradient) and that is fully accessible for SGD with backpropagation. There is no apparent reason not to make use of this information. The results, however, provide an initial understanding of both the quality and the speed of the ES approximations. 

ES improves quality with \textit{increasing parallelism} and it can exact lower computational cost, but even then the added computation cost is unnecessary for a supervised learning task. However, ES may prove to be the practical choice under certain circumstances, in constrained environments or in domains where a sufficient final performance is achievable with reasonable gradient approximations. 

In unsupervised or semi-supervised domains, SGD lacks the optimal gradient, and thus is becomes either unsuitable or completely not applicable (i.e. when the mathematics do not permit the computation of partial derivatives). The results from Salimans et al. \cite{2017arXiv170303864S} have shown that Reinforcement Learning does indeed benefit greatly replacing noisy SGD gradients with ES estimations. I think this is because ES can in principle approximate the ideal gradient, which is closer to the optimal than a noisy SGD. However, the benefits of ES in RL are not just that. Having the option to control the level of noise by adding or subtracting offspring from the population turns out to be a powerful tool. The technology today is tolerant of the computational requirements of ES and even more, the low-cost parallelism may imply a shorter wall-clock time as well. 

In conclusion, the main implication is not that ES can rival SGD, but that although ES is only guessing its gradients, it does a good job in a way that is easy to support with current technology and  it has great potential for improvement and the way - this is not only sufficient in some cases, but desirable in others.

Finally, my works on the topic is not only answering questions for the purpose of laying groundwork, but also raising questions for the same purpose. It provides an initial glimpse at the potential of ES.
